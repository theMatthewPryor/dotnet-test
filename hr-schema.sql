CREATE TABLE IF NOT EXISTS "User"(
  "ID" integer primary key autoincrement not null ,
  "FirstName" varchar ,
  "LastName" varchar ,
  "DepartmentID" integer
);

CREATE TABLE IF NOT EXISTS "Department"(
  "ID" integer primary key autoincrement not null ,
  "Name" varchar ,
  "HeadOfDepartmentUserID" integer
);